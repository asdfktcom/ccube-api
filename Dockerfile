FROM java:8-jre 

ENV VERTICLE_FILE ccube-api.war
ENV VERTICLE_HOME /usr/verticles

EXPOSE 8080
COPY build/libs/$VERTICLE_FILE $VERTICLE_HOME/

WORKDIR $VERTICLE_HOME
ENTRYPOINT ["sh", "-c"]
CMD ["exec java -jar $VERTICLE_FILE"]
