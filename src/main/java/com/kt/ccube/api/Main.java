package com.kt.ccube.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableCaching
@EnableScheduling
@SpringBootApplication
public class Main extends SpringBootServletInitializer implements CommandLineRunner {
	
    private static final Logger log = LoggerFactory.getLogger(Main.class);

    @Override
    public void run(String ... args) throws Exception {
        log.info("## Start Main!");
    }

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
}
