package com.kt.ccube.api.dto;

public class ContentsReq {
	
	private String bmTypeId;
	private String bmTypeNm;
	private String contsTypeId;
	private String contsTypeNm;
	private String cpId;
	private String cpNm;
	private String contsNm;
	private Integer page = 0;
	private Integer size = 10;
	
	public static ContentsReq of() {
		return new ContentsReq();
	}
	public String getBmTypeId() {
		return bmTypeId;
	}
	public ContentsReq setBmTypeId(String bmTypeId) {
		this.bmTypeId = bmTypeId;
		return this;
	}
	public String getBmTypeNm() {
		return bmTypeNm;
	}
	public ContentsReq setBmTypeNm(String bmTypeNm) {
		this.bmTypeNm = bmTypeNm;
		return this;
	}
	public String getContsTypeId() {
		return contsTypeId;
	}
	public ContentsReq setContsTypeId(String contsTypeId) {
		this.contsTypeId = contsTypeId;
		return this;
	}
	public String getContsTypeNm() {
		return contsTypeNm;
	}
	public ContentsReq setContsTypeNm(String contsTypeNm) {
		this.contsTypeNm = contsTypeNm;
		return this;
	}
	public String getCpId() {
		return cpId;
	}
	public ContentsReq setCpId(String cpId) {
		this.cpId = cpId;
		return this;
	}
	public String getCpNm() {
		return cpNm;
	}
	public ContentsReq setCpNm(String cpNm) {
		this.cpNm = cpNm;
		return this;
	}
	public String getContsNm() {
		return contsNm;
	}
	public ContentsReq setContsNm(String contsNm) {
		this.contsNm = contsNm;
		return this;
	}
	public Integer getPage() {
		return page;
	}
	public ContentsReq setPage(Integer page) {
		this.page = page;
		return this;
	}
	public Integer getSize() {
		return size;
	}
	public ContentsReq setSize(Integer size) {
		this.size = size;
		return this;
	}

}
