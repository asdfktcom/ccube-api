package com.kt.ccube.api.dto;

public class TemplateReq {

	private String bmTypeId;
	private String bmTypeNm;
	private String contsTypeId;
	private String contsTypeNm;
	private String tmpltNm;
	private Integer page = 0;
	private Integer size = 10;
	
	public static TemplateReq of() {
		return new TemplateReq();
	}
	public String getBmTypeId() {
		return bmTypeId;
	}
	public TemplateReq setBmTypeId(String bmTypeId) {
		this.bmTypeId = bmTypeId;
		return this;
	}
	public String getBmTypeNm() {
		return bmTypeNm;
	}
	public TemplateReq setBmTypeNm(String bmTypeNm) {
		this.bmTypeNm = bmTypeNm;
		return this;		
	}
	public String getContsTypeId() {
		return contsTypeId;
	}
	public TemplateReq setContsTypeId(String contsTypeId) {
		this.contsTypeId = contsTypeId;
		return this;		
	}
	public String getContsTypeNm() {
		return contsTypeNm;
	}
	public TemplateReq setContsTypeNm(String contsTypeNm) {
		this.contsTypeNm = contsTypeNm;
		return this;		
	}
	public String getTmpltNm() {
		return tmpltNm;
	}
	public TemplateReq setTmpltNm(String tmpltNm) {
		this.tmpltNm = tmpltNm;
		return this;
	}
	public Integer getPage() {
		return page;
	}
	public TemplateReq setPage(Integer page) {
		this.page = page;
		return this;		
	}
	public Integer getSize() {
		return size;
	}
	public TemplateReq setSize(Integer size) {
		this.size = size;
		return this;		
	}
	
}
