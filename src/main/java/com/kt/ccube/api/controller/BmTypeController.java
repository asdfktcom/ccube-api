package com.kt.ccube.api.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kt.ccube.api.service.BmTypeService;

@RestController
public class BmTypeController {
	
	@Autowired
	private BmTypeService bmTypeService;
	
	@GetMapping("/type/bm")
	public Object getBmTypeList(@RequestParam(value="bmTypeNm", required=false) String bmTypeNm) {
        return bmTypeService.getBmTypeList(null, bmTypeNm);
	}
	
	@GetMapping("/type/bm/{id}")
	public Map<String, Object> getBmType(@PathVariable(required=true) String id) {
        return bmTypeService.getBmType(id);
	}

}
