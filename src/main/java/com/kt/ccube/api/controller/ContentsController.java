package com.kt.ccube.api.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kt.ccube.api.dto.ContentsReq;
import com.kt.ccube.api.service.ContentsService;

@RestController
public class ContentsController {

	@Autowired
	private ContentsService contentsService;
	
	@GetMapping("/contents")
	public Object getContentsList(ContentsReq req) {
        return contentsService.getContentsList(req);
	}
	
	@GetMapping("/contents/{id}")
	public Map<String, Object> getContents(@PathVariable(required=true) String id) {
        return contentsService.getContents(id);
	}	
	
	@PostMapping("/contents")
	public Map<String, Object> postContents(@RequestBody Map<String, Object> contents) {
        return contentsService.postContents(contents);
	}
	
	@PutMapping("/contents/{id}")
	public Map<String, Object> putContents(@PathVariable(required=true) String id, @RequestBody Map<String, Object> contents) {
        return contentsService.putContents(id, contents);
	}
	
	@DeleteMapping("/contents/{id}")
	public Map<String, Object> deleteContents(@PathVariable(required=true) String id) {
        return contentsService.deleteContents(id);
	}
	
}
