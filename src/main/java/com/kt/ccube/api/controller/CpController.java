package com.kt.ccube.api.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kt.ccube.api.service.CpService;

@RestController
public class CpController {
	
	@Autowired
	private CpService cpService;

	@GetMapping("/cp")
	public Object getCpList(@RequestParam(value="cpNm", required=false) String cpNm) {
        return cpService.getCpList(null, cpNm);
	}
	
	@GetMapping("/cp/{id}")
	public Map<String, Object> getCp(@PathVariable(required=true) String id) {
        return cpService.getCp(id);
	}	
	
}
