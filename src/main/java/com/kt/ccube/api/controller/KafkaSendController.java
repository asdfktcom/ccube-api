package com.kt.ccube.api.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.ImmutableMap;
import com.kt.ccube.api.service.KafkaSendService;

@RestController
public class KafkaSendController {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    
    @Autowired
    private KafkaSendService kafkaSendService;
	
	@GetMapping("/kafka/send")
	public Object sendToKafka(
			@RequestParam(value="topic", required=false, defaultValue="test") String topic,
			@RequestParam(value="msg", required=false, defaultValue="test") String msg) {
		kafkaTemplate.send(topic, msg);
		return ImmutableMap.<String, Object>builder()
				.put("result", "0")
				.build();
	}
	
	@GetMapping("/kafka/send/collection/{collectionName}/{id}")
	public Map<String, Object> sendCollectionToKafka(@PathVariable(required=true) String collectionName, @PathVariable(required=true) String id) {
		return kafkaSendService.send(id, collectionName);
	}	

}
