package com.kt.ccube.api.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kt.ccube.api.dto.TemplateReq;
import com.kt.ccube.api.service.TemplateService;

@RestController
public class TemplateController {

	@Autowired
	private TemplateService templateService;
	
	@GetMapping("/template")
	public Object getTemplateList(TemplateReq req) {
        return templateService.getTemplateList(req);
	}
	
	@GetMapping("/template/{id}")
	public Map<String, Object> getTemplate(@PathVariable(required=true) String id) {
        return templateService.getTemplate(id);
	}
	
	@PostMapping("/template")
	public Map<String, Object> postContents(@RequestBody Map<String, Object> template) {
        return templateService.postTemplate(template);
	}
	
	@PutMapping("/template/{id}")
	public Map<String, Object> putContents(@PathVariable(required=true) String id, @RequestBody Map<String, Object> template) {
        return templateService.putTemplate(id, template);
	}
	
	@DeleteMapping("/template/{id}")
	public Map<String, Object> deleteContents(@PathVariable(required=true) String id) {
        return templateService.deleteTemplate(id);
	}	
	
}
