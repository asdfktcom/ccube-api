package com.kt.ccube.api.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kt.ccube.api.service.ContentsTypeService;

@RestController
public class ContentsTypeController {
	
	@Autowired
	private ContentsTypeService contentsTypeService;
	
	@GetMapping("/type/contents")
	public Object getContentsTypeList(@RequestParam(value="contsTypeNm", required=false) String contsTypeNm) {
		return contentsTypeService.getContentsTypeList(null, contsTypeNm);
	}
	
	@GetMapping("/type/contents/{id}")
	public Map<String, Object> getContentsType(@PathVariable(required=true) String id) {
		return contentsTypeService.getContentsType(id);
	}
}
