package com.kt.ccube.api.dao;

import static org.springframework.data.mongodb.core.query.Criteria.where;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.kt.ccube.api.dto.TemplateReq;

@Repository
public class TemplateDao extends CommonDao {
	
	private static final String COLLECTION = "TMPLT_BAS";	

	public List<Map<String, Object>> getTemplateList(TemplateReq req) {
		Query query = new Query().with(PageRequest.of(req.getPage(), req.getSize()));
		if (req.getBmTypeId() != null) {
			query.addCriteria(where("BM_TYPE_ID").is(req.getBmTypeId()));
		}
		if (req.getContsTypeId() != null) {
			query.addCriteria(where("CONTS_TYPE_ID").is(req.getContsTypeId()));
		}
		if (req.getTmpltNm() != null) {
			query.addCriteria(where("TMPLT_NM").regex(".*" + req.getTmpltNm() + ".*"));
		}
		return find(query, COLLECTION);
	}

	public Map<String, Object> getTemplate(String id) {
		return findById(id, COLLECTION);
	}
	
	public Map<String, Object> insertTemplate(Map<String, Object> template) {
		return insert(template, COLLECTION);
	}
	
	public Long updateTemplate(String id, Map<String, Object> template) {
		return update(id, template, COLLECTION);
	}
	
	public Long deleteTemplate(String id) {
		return remove(id, COLLECTION);
	}

}
