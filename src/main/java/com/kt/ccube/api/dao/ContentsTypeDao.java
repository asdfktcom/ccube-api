package com.kt.ccube.api.dao;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Maps;

@Repository
public class ContentsTypeDao extends CommonDao {
	
	private static final String COLLECTION = "CONTS_TYPE_BAS";
	
	public List<Map<String, Object>> getContentsTypeList(String id, String contentsTypeName) {
		return StringUtils.isNotBlank(id)
				? Arrays.asList(findById(id, COLLECTION))
				: StringUtils.isNotBlank(contentsTypeName)
					? find(query(where("CONTS_TYPE_NM").is(contentsTypeName)), COLLECTION)
					: find(new Query(), COLLECTION);		
	}
	
	public Map<String, Object> getContentsType(String id) {
		return findById(id, COLLECTION);
	}	
	
	public Map<String, Object> getContentsTypeByName(String contentsTypeName) {
		List<Map<String, Object>> result = getContentsTypeList(null, contentsTypeName);
		return CollectionUtils.size(result) > 0 ? result.get(0) : Maps.newHashMap();
	}
	
	public String getContentsTypeIdByName(String contentsTypeName) {
		return (String)MapUtils.getObject(getContentsTypeByName(contentsTypeName), "_id", "");
	}	

}
