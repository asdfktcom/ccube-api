package com.kt.ccube.api.dao;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;

@Repository
public class CommonDao {
	
	@Autowired
	protected MongoTemplate mongoTemplate;
	
	public Long getCount(Query query, String collectionName) {
		return mongoTemplate.count(query, collectionName);
	}
	
	public Long getTotalCount(String collectionName) {
		return getCount(new Query(), collectionName);
	}
	
	public Long getTotalPageCount(Integer size, String collectionName) {
		Long totalCount = getTotalCount(collectionName);
		return totalCount > 0 ? (totalCount - 1) / size + 1 : 0;
	}
	
	protected void updateCommonField(Map<String, Object> data, boolean isNew) {
		data.put("LAST_CHG_DT" , new Date(System.currentTimeMillis()));
		data.put("LAST_CHG_TRTR_ID"	, "system");
		if (isNew) {
			if (StringUtils.isBlank(MapUtils.getString(data, "_id"))) {
				data.put("_id", "X" + System.currentTimeMillis());				
			}
			data.put("FIRST_CRET_DT", new Date(System.currentTimeMillis()));
			data.put("FIRST_CRET_TRTR_ID", "system");
		} else {
			data.remove("_id");
			data.remove("FIRST_CRET_DT");
			data.remove("FIRST_CRET_TRTR_ID");
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> find(Query query, String collectionName) {
		Object result = mongoTemplate.find(query, new HashMap<String, Object>().getClass(), collectionName);
		return (List<Map<String, Object>>)result;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> findById(String id, String collectionName) {
		return mongoTemplate.findById(id, new HashMap<String, Object>().getClass(), collectionName);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> findByPage(Integer page, Integer size, String collectionName) {
		Query query = new Query().with(PageRequest.of(page, size));
		Object result = mongoTemplate.find(query, new HashMap<String, Object>().getClass(), collectionName);
		return (List<Map<String, Object>>)result;		
	}
	
	public Map<String, Object> insert(Map<String, Object> objectToSave, String collectionName) {
		updateCommonField(objectToSave, true);
		return mongoTemplate.insert(objectToSave, collectionName);
	}
	
	public Long update(String id, Map<String, Object> objectToSave, String collectionName) {
		updateCommonField(objectToSave, false);	
	    Update update = new Update();
		objectToSave.values().removeIf(Objects::isNull);
	    objectToSave.forEach(update::set);
	    UpdateResult result = mongoTemplate.updateMulti(query(where("_id").is(id)), update, collectionName);
		return result.getModifiedCount();
	}

	public Long remove(String id, String collectionName) {
		DeleteResult result = mongoTemplate.remove(query(where("_id").is(id)), collectionName);
		return result.getDeletedCount();
	}

}
