package com.kt.ccube.api.dao;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Maps;

@Repository
public class CpDao extends CommonDao {
	
	private static final String COLLECTION = "CP_BAS";
	
	public List<Map<String, Object>> getCpList(String id, String cpName) {
		return StringUtils.isNotBlank(id)
				? Arrays.asList(findById(id, COLLECTION))
				: StringUtils.isNotBlank(cpName)
					? find(query(where("CP_NM").is(cpName)), COLLECTION)
					: find(new Query(), COLLECTION);
	}
	
	public Map<String, Object> getCp(String id) {
		return findById(id, COLLECTION);
	}
	
	public Map<String, Object> getCpByName(String cpName) {
		List<Map<String, Object>> result = getCpList(null, cpName);
		return CollectionUtils.size(result) > 0 ? result.get(0) : Maps.newHashMap();
	}
	
	public String getCpIdByName(String cpName) {
		return (String)MapUtils.getObject(getCpByName(cpName), "_id", "");
	}	

}
