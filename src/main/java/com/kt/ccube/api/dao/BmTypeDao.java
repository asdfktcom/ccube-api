package com.kt.ccube.api.dao;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Maps;

@Repository
public class BmTypeDao extends CommonDao {

	private static final String COLLECTION = "BM_TYPE_BAS";

	public List<Map<String, Object>> getBmTypeList(String id, String bmTypeName) {
		return StringUtils.isNotBlank(id)
				? Arrays.asList(findById(id, COLLECTION))
				: StringUtils.isNotBlank(bmTypeName)
					? find(query(where("BM_TYPE_NM").is(bmTypeName)), COLLECTION)
					: find(new Query(), COLLECTION);
	}
	
	public Map<String, Object> getBmType(String id) {
		return findById(id, COLLECTION);
	}
	
	public Map<String, Object> getBmTypeByName(String bmTypeName) {
		List<Map<String, Object>> result = getBmTypeList(null, bmTypeName);
		return CollectionUtils.size(result) > 0 ? result.get(0) : Maps.newHashMap();
	}
	
	public String getBmTypeIdByName(String bmTypeName) {
		return (String)MapUtils.getObject(getBmTypeByName(bmTypeName), "_id", "");
	}
	
}
