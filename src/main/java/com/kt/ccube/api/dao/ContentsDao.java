package com.kt.ccube.api.dao;

import static org.springframework.data.mongodb.core.query.Criteria.where;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.kt.ccube.api.dto.ContentsReq;

@Repository
public class ContentsDao extends CommonDao {
	
	private static final String COLLECTION = "CONTS_BAS";

	public List<Map<String, Object>> getContentsList(ContentsReq req) {
		Query query = new Query().with(PageRequest.of(req.getPage(), req.getSize()));
		if (req.getBmTypeId() != null) {
			query.addCriteria(where("BM_TYPE_ID").is(req.getBmTypeId()));
		}
		if (req.getContsTypeId() != null) {
			query.addCriteria(where("CONTS_TYPE_ID").is(req.getContsTypeId()));
		}
		if (req.getCpId() != null) {
			query.addCriteria(where("CP_ID").is(req.getCpId()));
		}
		if (req.getContsNm() != null) {
			query.addCriteria(where("CONTS_NM").regex(".*" + req.getContsNm() + ".*"));
		}
		return find(query, COLLECTION);
	}

	public Map<String, Object> getContents(String id) {
		return findById(id, COLLECTION);
	}
	
	public Map<String, Object> insertContents(Map<String, Object> contents) {
		return insert(contents, COLLECTION);
	}
	
	public Long updateContents(String id, Map<String, Object> contents) {
		return update(id, contents, COLLECTION);
	}
	
	public Long deleteContents(String id) {
		return remove(id, COLLECTION);
	}	
	
}
