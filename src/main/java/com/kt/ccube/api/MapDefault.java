package com.kt.ccube.api;

import java.util.Map;

public class MapDefault {
	public static void check(Map<String, Object> map, String key, Object defaultValue) {
		if (map != null && map.get(key) == null) {
			map.put(key, defaultValue);
		}
	}
	public static void check(Map<String, Object> map, String key) {
		check(map, key, "");
	}	
}
