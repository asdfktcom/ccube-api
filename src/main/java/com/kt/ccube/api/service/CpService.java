package com.kt.ccube.api.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kt.ccube.api.dao.CpDao;

@Service
public class CpService {

	@Autowired
	private CpDao cpDao;	

	public Map<String, Object> getCp(String id) {
		return cpDao.getCp(id);
	}

	public List<Map<String, Object>> getCpList(String id, String cpName) {
		return cpDao.getCpList(id, cpName);
	}	
	
}
