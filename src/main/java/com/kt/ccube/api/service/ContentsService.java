package com.kt.ccube.api.service;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.ImmutableMap;
import com.kt.ccube.api.MapDefault;
import com.kt.ccube.api.dao.BmTypeDao;
import com.kt.ccube.api.dao.ContentsDao;
import com.kt.ccube.api.dao.ContentsTypeDao;
import com.kt.ccube.api.dao.CpDao;
import com.kt.ccube.api.dto.ContentsReq;

@Service
public class ContentsService {
	
	@Autowired
	private BmTypeDao bmTypeDao;
	
	@Autowired
	private ContentsTypeDao contentsTypeDao;
	
	@Autowired
	private CpDao cpDao;	
	
	@Autowired
	private ContentsDao contentsDao;
	
	@Autowired
	private KafkaSendService kafkaSendService;
	
	public List<Map<String, Object>> getContentsList(ContentsReq req) {
		if (StringUtils.isBlank(req.getBmTypeId()) && StringUtils.isNotBlank(req.getBmTypeNm())) {
			req.setBmTypeId(bmTypeDao.getBmTypeIdByName(req.getBmTypeNm()));
		}
		if (StringUtils.isBlank(req.getContsTypeId()) && StringUtils.isNotBlank(req.getContsTypeNm())) {
			req.setContsTypeId(contentsTypeDao.getContentsTypeIdByName(req.getContsTypeNm()));
		}
		if (StringUtils.isBlank(req.getCpId()) && StringUtils.isNotBlank(req.getCpNm())) {
			req.setCpId(cpDao.getCpIdByName(req.getCpNm()));
		}
		return contentsDao.getContentsList(req);
	}	
	
	public Map<String, Object> getContents(String id) {
		return contentsDao.getContents(id);
	}
	
	private Map<String, Object> checkDefault(Map<String, Object> contents) {
		MapDefault.check(contents, "CONTS_NM");
		MapDefault.check(contents, "BM_TYPE_ID");
		MapDefault.check(contents, "CONTS_TYPE_ID");
		MapDefault.check(contents, "CP_ID", "00000");
		MapDefault.check(contents, "CP_NM");
		MapDefault.check(contents, "CONTS_SPL_CONT_NM");
		MapDefault.check(contents, "SERS_ID");
		MapDefault.check(contents, "GENRE_ID", "000000");
		MapDefault.check(contents, "CONTS_DEVCH_LIST");
		MapDefault.check(contents, "MEDATA_LANG_CD");
		MapDefault.check(contents, "AUDIO_LANG_CD");
		MapDefault.check(contents, "SYNPS_SBST");
		MapDefault.check(contents, "CAST_SBST");
		MapDefault.check(contents, "DIRT_NM");
		MapDefault.check(contents, "MC_NM");
		return contents;
	}
	
	private void sendSettleData(String id) {
		kafkaSendService.send(id, "CONTS_BAS");
	}
	
	public Map<String, Object> postContents(Map<String, Object> contents) {
		String id =  "X" + System.currentTimeMillis();
		contents.put("_id", id);
		contents.put("CONTS_ID", id);
		Map<String, Object> result = contentsDao.insertContents(checkDefault(contents));
		sendSettleData(id);
		return result;
	}
	
	public Map<String, Object> putContents(String id, Map<String, Object> contents) {
		Map<String, Object> result = ImmutableMap.<String, Object>builder()
//				.put("count", contentsDao.updateContents(id, checkDefault(contents)))
				.put("count", contentsDao.updateContents(id, contents))				
				.build();
		sendSettleData(id);	
		return result;
	}
	
	public Map<String, Object> deleteContents(String id) {
		return ImmutableMap.<String, Object>builder()
				.put("count", contentsDao.deleteContents(id))
				.build();
	}
	
}
