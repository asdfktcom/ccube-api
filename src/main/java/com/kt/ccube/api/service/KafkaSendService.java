package com.kt.ccube.api.service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kt.ccube.api.dao.CommonDao;

@Service
public class KafkaSendService {
	
	private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    
    @Autowired
    private CommonDao commonDao;
    
	@Value("${ccube.topic:ccube}")
	private String topic;
	
	private Gson gson = new GsonBuilder().setPrettyPrinting().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
	
	private int send(List<Map<String, Object>> listData) {
    	if (CollectionUtils.isEmpty(listData)) {
    		log.info("## send data to kafka : data is EMPTY");
    	} else {
        	String data = gson.toJson(listData);
        	log.info("## send data to kafka : {}", data);
        	kafkaTemplate.send(topic, data);
    	}
    	return CollectionUtils.size(listData);
	}

    public Map<String, Object> send(String id, String collectionName) {
    	int sendCount = 0;
    	if(StringUtils.equals(id, "all")) {
    		int size = 50;
    		long totalCount = commonDao.getTotalCount(collectionName);
    		long totalPageCount = totalCount > 0 ? (totalCount - 1) / size + 1 : 0;
    		for (int page = 0 ; page < totalPageCount ; page++) {
    			sendCount = sendCount + send(commonDao.findByPage(page, size, collectionName));
    		}
    	} else {
    		Map<String, Object> map = commonDao.findById(id, collectionName);
    		map.entrySet().stream().forEach(entry -> {
    			String key = entry.getKey();
    			Object value = entry.getValue();
    			String valueClassName = value == null ? null : value.getClass().getCanonicalName();
    			System.out.println(String.format("## key=%s, value=%s, valueClassName=%s", key, value, valueClassName));
    		});
    		if (MapUtils.isEmpty(map)) {
    			log.info("## send data to kafka, selected data is empty : {}", id);
    		} else {
    			sendCount = send(Arrays.asList(map));	
    		}
    	}
    	return ImmutableMap.<String, Object>builder().put("count", sendCount).build();
    }
}
