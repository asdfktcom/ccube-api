package com.kt.ccube.api.service;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.kt.ccube.api.MapDefault;
import com.kt.ccube.api.dao.BmTypeDao;
import com.kt.ccube.api.dao.ContentsTypeDao;
import com.kt.ccube.api.dao.TemplateDao;
import com.kt.ccube.api.dto.TemplateReq;

@Service
public class TemplateService {
	
	@Autowired
	private BmTypeDao bmTypeDao;
	
	@Autowired
	private ContentsTypeDao contentsTypeDao;	
	
	@Autowired
	private TemplateDao templateDao;

	public Object getTemplateList(TemplateReq req) {
		if (StringUtils.isBlank(req.getBmTypeId()) && StringUtils.isNotBlank(req.getBmTypeNm())) {
			req.setBmTypeId(bmTypeDao.getBmTypeIdByName(req.getBmTypeNm()));
		}
		if (StringUtils.isBlank(req.getContsTypeId()) && StringUtils.isNotBlank(req.getContsTypeNm())) {
			req.setContsTypeId(contentsTypeDao.getContentsTypeIdByName(req.getContsTypeNm()));
		}
		return templateDao.getTemplateList(req);
	}

	public Map<String, Object> getTemplate(String id) {
		return templateDao.getTemplate(id);
	}
	
	private Map<String, Object> checkDefault(Map<String, Object> template) {
		MapDefault.check(template, "TMPLT_NM");
		MapDefault.check(template, "BM_TYPE_ID");
		MapDefault.check(template, "CONTS_TYPE_ID");
		MapDefault.check(template, "TMPLT_VER_NO", "1");
		MapDefault.check(template, "TMPLT_SBST", Maps.newHashMap());
		return template;
	}
	
	public Map<String, Object> postTemplate(Map<String, Object> template) {
		template.put("_id", "0" + System.currentTimeMillis());		
		return templateDao.insertTemplate(checkDefault(template));
	}	
	
	public Map<String, Object> putTemplate(String id, Map<String, Object> template) {
		return ImmutableMap.<String, Object>builder()
//				.put("count", templateDao.updateTemplate(id, checkDefault(template)))
				.put("count", templateDao.updateTemplate(id, template))
				.build();
	}
	
	public Map<String, Object> deleteTemplate(String id) {
		return ImmutableMap.<String, Object>builder()
				.put("count", templateDao.deleteTemplate(id))
				.build();
	}
	
}
