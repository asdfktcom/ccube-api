package com.kt.ccube.api.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kt.ccube.api.dao.ContentsTypeDao;

@Service
public class ContentsTypeService {

	@Autowired
	private ContentsTypeDao contentsTypeDao;	

	public Map<String, Object> getContentsType(String id) {
		return contentsTypeDao.getContentsType(id);
	}

	public List<Map<String, Object>> getContentsTypeList(String id, String contentsTypeName) {
		return contentsTypeDao.getContentsTypeList(id, contentsTypeName);
	}		
	
}
