package com.kt.ccube.api.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kt.ccube.api.dao.BmTypeDao;

@Service
public class BmTypeService {
	
	@Autowired
	private BmTypeDao bmTypeDao;	

	public Map<String, Object> getBmType(String id) {
		return bmTypeDao.getBmType(id);
	}

	public List<Map<String, Object>> getBmTypeList(String id, String bmTypeName) {
		return bmTypeDao.getBmTypeList(id, bmTypeName);
	}		

}
