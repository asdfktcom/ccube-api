package com.kt.ccube.api.model;

public class ContentType {
	
	private String contentTypeId;
	private String description;
	
	public ContentType of() {
		return new ContentType();
	}
	public String getContentTypeId() {
		return contentTypeId;
	}
	public void setContentTypeId(String contentTypeId) {
		this.contentTypeId = contentTypeId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
