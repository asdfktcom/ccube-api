package com.kt.ccube.api.model;

public class Template {

	private String templateId;
	private String templateSchema;
	private String version;
	
	public static Template of() {
		return new Template();
	}
	public String getTemplateId() {
		return templateId;
	}
	public Template setTemplateId(String templateId) {
		this.templateId = templateId;
		return this;
	}
	public String getTemplateSchema() {
		return templateSchema;
	}
	public Template setTemplateSchema(String templateSchema) {
		this.templateSchema = templateSchema;
		return this;
	}
	public String getVersion() {
		return version;
	}
	public Template setVersion(String version) {
		this.version = version;
		return this;
	}
	
}
