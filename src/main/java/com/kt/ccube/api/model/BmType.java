package com.kt.ccube.api.model;

public class BmType {
	
	private String bmTypeId;
	private String bmName;
	private String description;
	
	public static BmType of() {
		return new BmType();
	}
	public String getBmTypeId() {
		return bmTypeId;
	}
	public BmType setBmTypeId(String bmTypeId) {
		this.bmTypeId = bmTypeId;
		return this;
	}
	public String getBmName() {
		return bmName;
	}
	public BmType setBmName(String bmName) {
		this.bmName = bmName;
		return this;
	}
	public String getDescription() {
		return description;
	}
	public BmType setDescription(String description) {
		this.description = description;
		return this;
	}
	
}
