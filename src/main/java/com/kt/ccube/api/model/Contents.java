package com.kt.ccube.api.model;

public class Contents {
	
	private String contentsId;
	private String contentsName;
	private String contsNm;
	private String contentTypeId;
	private String cpId;
	private String bmTypeId;	
	private String templateId;
	
	public String getContsNm() {
		return contsNm;
	}
	public void setContsNm(String contsNm) {
		this.contsNm = contsNm;
	}
	public static Contents of() {
		return new Contents();
	}
	public String getContentsId() {
		return contentsId;
	}
	public Contents setContentsId(String contentsId) {
		this.contentsId = contentsId;
		return this;
	}
	public String getContentsName() {
		return contentsName;
	}
	public Contents setContentsName(String contentsName) {
		this.contentsName = contentsName;
		return this;
	}
	public String getContentTypeId() {
		return contentTypeId;
	}
	public Contents setContentTypeId(String contentTypeId) {
		this.contentTypeId = contentTypeId;
		return this;
	}
	public String getCpId() {
		return cpId;
	}
	public Contents setCpId(String cpId) {
		this.cpId = cpId;
		return this;
	}
	public String getBmTypeId() {
		return bmTypeId;
	}
	public Contents setBmTypeId(String bmTypeId) {
		this.bmTypeId = bmTypeId;
		return this;
	}
	public String getTemplateId() {
		return templateId;
	}
	public Contents setTemplateId(String templateId) {
		this.templateId = templateId;
		return this;
	}

}
